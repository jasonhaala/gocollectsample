<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">
    <title>Sample App<?= empty($pageTitle) ? '' : ' :: '.$pageTitle; ?></title>
    <meta name="description" content="">

    <link href="/pkg/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    <link href="/pkg/Ionicons/css/ionicons.css" rel="stylesheet">

    <link rel="stylesheet" href="/css/slim.css">
    @yield('styles')
</head>
<body>

@include('partials.headers.main_app_header')
@include('partials.nav.app_header_nav')

<div class="slim-mainpanel">
    <div class="container">
        <div class="slim-pageheader">
            <span class="breadcrumb slim-breadcrumb"></span>
            <h6 class="slim-pagetitle"><?= $pageTitle ?></h6>
        </div>

        @yield('content')

    </div>
</div>


<script src="/pkg/jquery/js/jquery.js"></script>
<script src="/pkg/popper.js/js/popper.js"></script>
<script src="/pkg/bootstrap/js/bootstrap.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="/js/slim.js"></script>
@yield('scripts')

</body>
</html>
