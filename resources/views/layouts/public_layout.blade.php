<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sample App<?= empty($pageTitle) ? '' : ' :: '.$pageTitle; ?></title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/slim.css">
    @yield('styles')
</head>
<body>

@yield('content')

<script src="/pkg/jquery/js/jquery.js"></script>
<script src="/pkg/popper.js/js/popper.js"></script>
<script src="/pkg/bootstrap/js/bootstrap.js"></script>
<script src="/js/sweetalert.min.js"></script>
<script src="/js/slim.js"></script>
@yield('scripts')

</body>
</html>
