<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/app">
                    <i class="icon ion-ios-analytics-outline"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item with-sub">
                <a class="nav-link" href="#">
                    <i class="icon ion-ios-home-outline"></i>
                    <span>Comics</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li><a href="#">Hottest Comics</a></li>
                        <li><a href="#">CGC Cert Lookup</a></li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="icon ion-ios-analytics-outline"></i>
                    <span>Blog</span>
                </a>
            </li>
        </ul>
    </div>
</div>
