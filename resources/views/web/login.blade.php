@extends('layouts.public_layout', ['pageTitle' => empty($pageTitle) ? 'Login' : $pageTitle])

@section('content')
    <div class="signin-wrapper">
        <div class="signin-box">
            <h2 class="signin-title-primary">Welcome back!</h2>
            <h3 class="signin-title-secondary">Sign in to continue.</h3>
            <form method="post" action="/login">
                <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>

                @include('partials.alerts.errors')

                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Enter your username" value="<?= old('username'); ?>">
                </div>
                <div class="form-group mg-b-50">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-signin">Login</button>
                <p class="mg-t-40 mg-b-0">Don't have an account? <a href="/join">Signup Here</a></p>
            </form>
        </div>
    </div>
@stop
