@extends('layouts.public_layout', ['pageTitle' => empty($pageTitle) ? 'Join Free' : $pageTitle])

@section('content')
    <div class="signin-wrapper">

        <form method="post" action="/join" class="signin-box signup">
            <input type="hidden" name="_token" value="<?= csrf_token(); ?>"/>

            <h3 class="signin-title-primary">Get Started!</h3>
            <h5 class="signin-title-secondary lh-4">It's free to signup and only takes a minute.</h5>

            @include('partials.alerts.errors')

            <div class="row row-xs mg-b-10">
                <div class="col-sm-12">
                    <label class="block mg-b-0">Enter your name</label>
                </div>
                <div class="col-sm">
                    <input type="text" name="first_name" class="form-control" placeholder="First Name" value="<?= old('first_name'); ?>">
                </div>
                <div class="col-sm">
                    <input type="text" name="last_name" class="form-control" placeholder="Last Name" value="<?= old('last_name'); ?>">
                </div>
            </div>

            <div class="row mg-b-10">
                <div class="col-sm">
                    <label class="mg-b-0">Enter a username</label>
                    <input type="text" name="username" class="form-control" placeholder="" value="<?= old('username'); ?>">
                </div>
            </div>

            <div class="row mg-b-10">
                <div class="col-sm">
                    <label class="mg-b-0">Enter your email address</label>
                    <input type="email" name="email" class="form-control" placeholder="" value="<?= old('email'); ?>">
                </div>
            </div>

            <div class="row mg-b-10">
                <div class="col-sm">
                    <label class="mg-b-0">Enter a password</label>
                    <input type="password" name="password" class="form-control" placeholder="">
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-block btn-signin">Join Free</button>

            <div class="signup-separator"><span>or signup using</span></div>

            <button type="button" class="btn btn-facebook btn-block">Sign Up Using Facebook</button>

            <p class="mg-t-40 mg-b-0">Already have an account? <a href="/login">Login Here</a></p>
        </form>

    </div>
@stop

