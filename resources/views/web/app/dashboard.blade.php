@extends('layouts.app_layout', ['pageTitle' => empty($pageTitle) ? 'Dashboard' : $pageTitle])

@section('content')
    <h4>Welcome to the app, {{ $appUser->first_name }}!</h4>
@stop

