@extends('layouts.app_layout', ['pageTitle' => empty($pageTitle) ? 'My Account Details' : $pageTitle])

@section('styles')
    <link href="/pkg/jquery-toggles/css/toggles-full.css" rel="stylesheet">
@stop

@section('content')
    <div class="card">
        <div id="acctDetailsForm" class="card-body pd-30">
            <div class="form-group">
                <div class="row row-sm">
                    <div class="col-sm">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control firstname-field" placeholder="" value="<?= $user->first_name; ?>">
                    </div>
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control lastname-field" placeholder="" value="<?= $user->last_name; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row row-sm">
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" disabled placeholder="" value="<?= old('username', $user->username); ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row row-sm">
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                        <label>Email Address</label>
                        <input type="email" name="email" class="form-control email-field" placeholder="" value="<?= old('email', $user->email); ?>">
                    </div>
                </div>
            </div>
            <div class="form-group mg-b-0">
                <div class="row">
                    <div class="col-sm mg-t-20 mg-sm-t-0">
                        <label class="mg-r-10">Change Password</label>
                        <div class="toggle-wrapper align-middle">
                            <div id="passwordToggle" class="toggle toggle-light success"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="passwordFormGroup" class="form-group" style="display: none;">
                <div class="row">
                    <div class="col-sm-12 mg-t-20">
                        <label>Current Password</label>
                        <input type="password" name="current_password" class="form-control password-field current-password" placeholder="">
                    </div>
                    <div class="col-md-6 mg-t-20">
                        <label>New Password</label>
                        <input type="password" name="new_password" class="form-control password-field new-password" placeholder="">
                    </div>
                    <div class="col-md-6 mg-t-20">
                        <label>Repeat New Password</label>
                        <input type="password" name="new_password_confirmed" class="form-control password-field new-password2" placeholder="">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row mg-t-40">
                    <button id="saveAccountBtn" class="btn btn-oblong btn-primary col-md-6 offset-md-3" style="display: none;">Update My Profile</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script src="/js/dirrty.js"></script>
    <script src="/pkg/jquery-toggles/js/toggles.min.js"></script>
    <script>
        $(function() {
            'use strict'

            // Will be used to prevent multiple form submissions.
            var processingForm = false;

            // Determines whether to submit the password fields.
            var updatePassword = false;

            // Initialize password toggle
            $('#passwordToggle').toggles({
                on:     false,
                height: 26,
                text: {
                    on: 'Yes',
                    off: 'No'
                },
            });


            // Hide/show the password fields when toggled.
            $('#passwordToggle').on('toggle', function(e, active) {
                if (active) {
                    $('#passwordFormGroup').show();
                    updatePassword = true;
                } else {
                    $('#passwordFormGroup').hide();
                    updatePassword = false;
                }
            });


            // Determine if any form fields have been modified to show/hide the update button
            $('#acctDetailsForm').dirrty({
                preventLeaving: false
            }).on('dirty', function() {
                // Field(s) have been modified.
                $('#saveAccountBtn').show();
            }).on("clean", function() {
                // Form data is unchanged.
                $('#saveAccountBtn').hide();
            });


            // On-click handler for the save account details button
            $('#saveAccountBtn').click(function(e) {
                if (processingForm) {
                    console.log('Already being processed.')
                    e.preventDefault();
                    return false;
                }

                var postData = {
                    _token: '<?= csrf_token(); ?>',
                    user_id: '<?= $user->id; ?>',
                    first_name: $('.firstname-field').val(),
                    last_name: $('.lastname-field').val(),
                    email: $('.email-field').val()
                };

                // Pre-validate the required fields
                if (! postData.first_name) {
                    swal('Oops', 'First name is required.', 'error');
                    return false;
                } else if (! postData.email) {
                    swal('Oops', 'An email address is required.', 'error');
                    return false;
                }

                if (updatePassword) {
                    postData.current_password = $('.current-password').val();
                    postData.new_password = $('.new-password').val();
                    postData.new_password_confirmation = $('.new-password2').val();

                    if (! postData.current_password) {
                        swal('Oops', 'Enter your current password.', 'error');
                        return false;
                    }
                    if (! postData.new_password) {
                        swal('Oops', 'Enter a new password.', 'error');
                        return false;
                    }
                    if (postData.new_password !== postData.new_password_confirmation) {
                        swal('Oops', 'The new passwords do not match.', 'error');
                        return false;
                    }
                }

                return saveUserAccountDetails(postData);
            });
        });


        /**
         * Processes the account details form
         *
         * @param array postData
         */
        function saveUserAccountDetails(postData) {
            processingForm = true;

            $.post('/app/account/details', postData, function(response) {
                 if (response.success) {
                     swal('Success!', 'Your account details have been updated.' , 'success');
                 } else {
                     // Display an error message
                     var errorMsg = 'Account update failed.';
                     var error = response.error || null;
                     console.log(response.error);
                     if (typeof error === 'string' || error instanceof String) {
                         errorMsg = error;
                     } else if (error.email) {
                         errorMsg = error.email['0'];
                     } else if (error.first_name) {
                         errorMsg = error.first_name['0'];
                     } else if (error.first_name) {
                         errorMsg = error.last_name['0'];
                     } else if (error.current_password) {
                         errorMsg = error.current_password['0'];
                     } else if (error.new_password) {
                         errorMsg = error.new_password['0'];
                     } else if (error.new_password2) {
                         errorMsg = error.new_password2['0'];
                     }
                     swal('Oops', errorMsg , 'error');
                     return false;
                 }
             })
             .fail(function() {
                 swal('Oops', 'There was a problem submitting your data.' , 'error');
             })
             .always(function() {
                 processingForm = false;
             });
        }
    </script>
@stop
