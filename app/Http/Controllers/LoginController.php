<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Helpers\SessionData;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/app';


    public function __construct()
    {
        // Remove auth middleware restriction for the logout route
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'username';
    }


    public function index()
    {
        return view('web.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        # Validate the input
        $this->validateLogin($request);

        $loginData = [
            'login_uri' => $request->path(),
            'username'  => $request->input('username'),
        ];

        if ($this->hasTooManyLoginAttempts($request)) {
            # Potential brute force attack. Temporarily disable allowing the user to login.
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // Attempt to authenticate the user
        if ($this->attemptLogin($request)) {
            # Authentication was successful
            # Set the user's data in session
            $user = (new User())->findByUsername($loginData['username']);
            if (empty($user->id)) {
                # User was not found.
                # Logout to clear any cached authentication data & return user to login form.
                return $this->logout($request);
            }

            # Store the user's data in session
            SessionData::setUser($user);

            $request->session()->regenerate();

            $this->clearLoginAttempts($request);

            return redirect($this->redirectTo);
        }

        # Authentication failed.
        # Increment the number of login attempts and take the user back to the login form.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Log the user out of the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        SessionData::logoutUser();

        return redirect('/login');
    }


    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed                    $user
     *
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return true;
    }
}
