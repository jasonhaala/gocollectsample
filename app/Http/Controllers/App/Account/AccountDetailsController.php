<?php

namespace App\Http\Controllers\App\Account;

use App\Helpers\SessionData;
use App\Http\Controllers\App\AppController;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AccountDetailsController extends AppController
{
    public function index()
    {
        $user = SessionData::getUser();

        return view('web.app.account.details', compact('user'));
    }

    public function store(Request $request)
    {
        Log::info('Input:');
        Log::info($request->all());

        # Check if the password is being updated
        $updatePassword = $request->has('new_password') ? true : false;

        # Validate the input data
        $validator = $this->validator($request->all(), $updatePassword);
        if ($validator->fails()) {
            Log::info('Validation failed');
            Log::info($validator->errors());

            return response()->json(['error' => $validator->errors()]);
        }

        # Initial input validation passed.
        # Retrieve the user to be updated.
        $userId = $request->input('user_id');
        $user = User::find($userId);

        Log::info('User password:');
        Log::info($user->password);
        # If updating the password, check if the typed password matches the current set password.
        if ($updatePassword) {
            # Handle password validation checks
            if (Hash::check($request->input('current_password'), $user->password)) {
                # Passwords match...
                Log::info('Passwords Match');
            } else {
                # Incorrect password. Return without saving.

                return response()->json(['error' => 'Current password is incorrect.']);
            }
        }

        # Update the user's account details
        $updatedUser = $this->updateAccountDetails($user, $request->all());
        Log::info('Updated User:');
        Log::info($updatedUser);

        if ($updatedUser) {
            # Update the data stored in session.
            SessionData::setUser($updatedUser);
        } else {
            # Update failed.
            return response()->json(['error' => 'Unable to update account details.']);
        }

        return response()->json(['success' => true]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array   $input
     * @param  integer $userId
     * @param  boolean $updatePassword
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator($input, $userId, $updatePassword = false)
    {
        $rules = [
            'first_name' => 'required|string|between:2,45',
            'last_name'  => 'nullable|string|between:2,45',
            'email'      => 'required|string|email|max:100', Rule::unique('users')->ignore($userId)
        ];

        # Validate the new password if it's being updated
        if ($updatePassword) {
            $rules['new_password'] = 'required|string|min:5|confirmed';
        }

        return Validator::make($input, $rules);
    }


    /**
     * Update the user's account details
     *
     * @param \App\Models\User $user
     * @param array $data
     *
     * @return \App\Models\User
     */
    protected function updateAccountDetails(User $user, $data)
    {
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->email = $data['email'];
        if (! empty($data['new_password'])) {
            $user->password = bcrypt($data['new_password']);
        }

        $user->save();

        return $user;
    }
}
