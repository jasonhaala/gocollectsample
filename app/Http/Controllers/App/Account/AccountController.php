<?php

namespace App\Http\Controllers\App\Account;

use App\Http\Controllers\App\AppController;

class AccountController extends AppController
{
    public function index()
    {
        return redirect('/app/account/details');
    }

}
