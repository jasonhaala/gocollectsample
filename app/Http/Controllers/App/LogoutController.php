<?php

namespace App\Http\Controllers\App;

use App\Http\Requests;
use App\Helpers\SessionData;

class LogoutController extends AppController
{
    public function index()
    {
        SessionData::logoutUser();

        return redirect('/login');
    }
}
