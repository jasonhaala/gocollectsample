<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Http\Requests;

class AppController extends Controller
{
    protected $appHomeRedirect = '/app';

    public function dashboard()
    {
        return view('web.app.dashboard');
    }
}
