<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Helpers\SessionData;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/app';


    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Shows the registration form
     */
    public function index()
    {
        return $this->showRegistrationForm();
    }

    /**
     * Handles user registration request.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->register($request);
    }

    /**
     * Returns the view for the registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('web.join');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        # Validate the form input
        $validator = $this->validator($request->all());
        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator);
        }

        # Create the new user
        $user = $this->create($request->all());

        # Authenticate the user and log them into the app
        $this->guard()->login($user);

        # Store the user's data in session
        SessionData::setUser($user);

        return redirect($this->redirectTo);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $input
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $input)
    {
        return Validator::make($input, [
            'first_name' => 'required|string|between:2,45',
            'last_name'  => 'string|between:2,45',
            'username'   => 'required|string|between:4,20|unique:users',
            'email'      => 'required|string|email|max:100|unique:users',
            'password'   => 'required|string|min:5',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name'  => $data['last_name'] ?? null,
            'username'   => $data['username'],
            'email'      => $data['email'],
            'password'   => bcrypt($data['password']),
            'is_active'  => true,
            'is_admin'   => false
        ]);
    }
}
