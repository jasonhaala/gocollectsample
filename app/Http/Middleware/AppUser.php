<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\SessionData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AppUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $appUser = SessionData::getUser();
        if (empty($appUser)) {
            # Unable to find the user's session data.
            # Log them out to force a reauthentication
            SessionData::logoutUser();

            return redirect('/login');
        }

        # Share the office user's data across views
        View::share('appUser', $appUser);

        return $next($request);
    }
}
