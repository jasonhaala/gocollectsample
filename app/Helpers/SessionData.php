<?php

namespace App\Helpers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SessionData
{
    /**
     * Returns the user stored in session
     * This can return the entire user object, or the value of specific field name
     *
     * @param string|null $field
     *
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed
     */
    public static function getUser($field = null)
    {
        // Check if the user data already exists in session
        $user = session('user');
        if (empty($field)) {
            // A specific field isn't being requested.
            // Return the entire user object.
            return $user;
        }

        // Return the specific user field being requested
        return $user->$field;
    }

    /**
     * Store the User object in session
     *
     * @param \App\Models\User $user
     */
    public static function setUser(User $user)
    {
        $userData = new \stdClass();
        $userData->id = $user->id;
        $userData->first_name = $user->first_name;
        $userData->last_name = $user->last_name;
        $userData->username = $user->username;
        $userData->email = $user->email;
        $userData->is_admin = $user->is_admin;

        session(['user' => $userData]);
    }

    /**
     * Returns the URL the user was originally intending to visit prior to requiring authentication.
     * Choosing to forget the URL will remove the data stored in session once it's no longer needed.
     *
     * @param bool $forget
     *
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed
     */
    public static function getLoginRedirectUrl($forget = false)
    {
        $redirectUrl = session('loginRedirectUrl');
        if ($forget) {
            self::forgetLoginRedirectUrl();
        }

        return $redirectUrl;
    }

    /**
     * Sets the URL the user was originally intending to visit prior to requiring authentication
     *
     * @param string $url
     *
     * @return \Illuminate\Session\SessionManager|\Illuminate\Session\Store|mixed
     */
    public static function setLoginRedirectUrl($url)
    {
        session(['loginRedirectUrl' => $url]);
    }

    public static function forgetLoginRedirectUrl()
    {
        session()->forget('loginRedirectUrl');
    }

    /**
     * Logout the user by flushing session data and deauthenticating
     */
    public static function logoutUser()
    {
        Auth::logout();

        session()->flush();
        session()->regenerate();
    }

}
