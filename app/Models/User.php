<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [ 'id' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * Find a single user by their unique id
     *
     * @param  integer $userId
     * @return User
     */
    public function findById($userId)
    {
        return $this->where('id', $userId)->first();
    }

    /**
     * Find a single user by email
     *
     * @param  string $email
     * @return User
     */
    public function findByEmail($email)
    {
        return $this->where('email', $email)->first();
    }

    /**
     * Find a single user by username
     *
     * @param  string $username
     * @return User
     */
    public function findByUsername($username)
    {
        return $this->where('username', $username)->first();
    }

    public function updateUserById($userId, $data)
    {
        return DB::table($this->table)
            ->where('id', $userId)
            ->update($data);
    }


    /*
     * Returns the User object for the authenticated user, using Session and Auth
     */
    public function authUser()
    {
        $user = session('user');
        if (empty($user)) {
            # Authenticate the user & set in session
            $user = auth()->user();
            SessionData::setUser($user);
        }

        return $user;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

}
