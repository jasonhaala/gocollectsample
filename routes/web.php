<?php

/*
|--------------------------------------------------------------------------
| Global Routes (Available To All)
|--------------------------------------------------------------------------
*/
//

/*
|--------------------------------------------------------------------------
| Unauthenticated Visitor Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'guest'], function() {
    Route::resource('login', 'LoginController', [ 'only' => ['index', 'store'], 'names' => ['index' => 'login'] ]);
    Route::resource('join', 'RegisterController', [ 'only' => ['index', 'store'] ]);
    Route::get('/', 'LanderController@index');
});


/*
|--------------------------------------------------------------------------
| Authenticated User Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'app', 'namespace' => 'App', 'middleware' => ['auth', 'appUser']], function () {
    Route::get('/', 'AppController@dashboard');
    Route::get('logout', 'LogoutController@index');

    # User Account
    Route::group(['prefix' => 'account', 'namespace' => 'Account'], function () {
        Route::get('/', 'AccountController@index');
        Route::resource('details', 'AccountDetailsController', ['only' => ['index', 'store']]);
    });
});
