<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    private $table = 'users';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # Create a test user if no users exists
        $existingUser = DB::table($this->table)->first();
        if (! $existingUser) {
            DB::table($this->table)->insert([
                'first_name' => 'Crash',
                'last_name'  => 'Test-Dummy',
                'username'   => 'crash',
                'email'      => 'crash@testdummy.com',
                'is_admin'   => false,
                'is_active'  => true,
                'password'   => bcrypt('test'),
            ]);
        }
    }
}
